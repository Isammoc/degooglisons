<?php

$data = [
    'tip2017' => [
        'name'          => '180 000€ + 1 250 donateurs récurrents',                                          /* Dons */
        'wkp'           => '',
        'description'   => 'Objectifs',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '11 permanents',                                          /* Salariés */
        'date_frama'    => '2017',
        'coordonnees'   => '',
        'class'         => 'objectifs',
        'soft_frama'    => '',
        'long_desc'     => 'Pour mener à bien ces projets, nos besoins représente 2,27 sec de CA quotidien de Google', /* Comparaison Google CA */
        'url_frama'     => '',
        'id_frama'      => 'tip2017',
        'modale_title'  => 'Nos objectifs pour 2017',
        'modale_body'   => '<p>Nous toucherons au but ! Il restera encore des services « sensibles » à mettre en place, notamment les services liés à la messagerie (liste de diffusion, et bien entendu emails).</p>
                            <p>Non seulement ces services sont les plus coûteux à mettre en place (il est plus simple de gérer un site qui fait un million de visites par jour que de gérer 10 000 boîtes mails), mais il faudra aussi (surtout !) assurer le bon fonctionnement (maintenance, mise à jour, support, etc.) des projets mis en place depuis 2011.</p>
                            <p>Cela nécessitera le recrutement de 3 nouvelles personnes (essentiellement sur les aspects techniques).</p>
                            <p>Pour vous donner une échelle de nos besoins, notre objectif 2017 (180 000€ et 1 250 donateurs récurrents) représente le coût de 54 mètres d’autoroute, ou 0,0006% du C.A. annuel de Google</p>',
        'modale_footer' => '<a href="http://soutenir.framasoft.org" class="btn btn-soutenir"><i class="fa fa-fw fa-heart"></i> Soutenir</a>'
    ],
    'tip2016' => [
        'name'          => '130 000€ + 1 080 donateurs récurrents',
        'wkp'           => '',
        'description'   => 'Objectifs',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '8 permanents',
        'date_frama'    => '2016',
        'coordonnees'   => '',
        'class'         => 'objectifs',
        'soft_frama'    => '',
        'long_desc'     => 'Pour mener à bien ces projets, nos besoins représentent 2,27 sec de CA quotidien de Google',
        'url_frama'     => '',
        'id_frama'      => 'tip2016',
        'modale_title'  => 'Nos objectifs pour 2016',
        'modale_body'   => '<p>Début 2016, Framasoft comptera déjà plus d’une vingtaine de services en ligne. Mais nous ne comptons pas nous arrêter là ! Nous souhaitons en effet proposer de nouvelles alternatives libres aux applications des géants d’Internet. Notamment :</p>
                            <ul>
                                <li>un service d’agenda partagé (en alternative à Google Agenda)</li>
                                <li>un service de microblogging (en alternative à Twitter)</li>
                                <li>un service d’hébergement de vidéos libres (en alternative à Youtube)</li>
                            </ul>
                            <p>Pour assurer la mise en place de ces projets ainsi que maintenir les projets existants, l’association aura besoin de recruter 3 nouvelles personnes (un développeur, un second adminsys, et un administrateur). </p>
                            <p>Afin de ne pas systématiquement solliciter nos gentils donnateurs, une partie de la somme nécessaire pourra provenir de subventions.</p>
                            <p>Pour vous donner une échelle de nos besoins, notre objectif 2016 (130 000€ et 1 080 donateurs récurrents) représente le coût de 38 mètres d’autoroute, ou 0,0004% du C.A. annuel de Google</p>',
        'modale_footer' => '<a href="http://soutenir.framasoft.org" class="btn btn-soutenir"><i class="fa fa-fw fa-heart"></i> Soutenir</a>'
    ],
    'tip2015' => [
        'name'          => '70 000€ + 780 donateurs récurrents',
        'wkp'           => '',
        'description'   => 'Objectifs',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '5 permanents',
        'date_frama'    => '2015',
        'coordonnees'   => '',
        'class'         => 'objectifs',
        'soft_frama'    => '',
        'long_desc'     => 'Pour mener à bien ces projets, nos besoins représentent 2,27 sec de CA quotidien de Google',
        'url_frama'     => '',
        'id_frama'      => 'tip2015',
        'modale_title'  => 'Nos objectifs pour 2015',
        'modale_body'   => '<p>Framasoft souhaite mettre à disposition plusieurs services en ligne importants en 2015 :</p>
                            <ul>
                                <li>un service de d’hébergement de documents (en alternative à Dropbox)</li>
                                <li>un service d’envoi de gros fichiers (en alternative à WeTransfer)</li>
                                <li>un service de présentations/diaporama en ligne (un « PowerPoint en ligne », si vous préférez)</li>
                                <li>un service de visioconférence (en alternative à Skype), directement depuis votre navigateur, sans installation de logiciel</li>
                            </ul>
                            <p>Pour assurer la mise en place de ces projets ainsi que maintenir les projets existants, l’association aura besoin d’embaucher, en plus de ses permanents actuels, un administrateur systèmes à temps plein, ainsi que 2 stagiaires. </p>
                            <p>Pour vous donner une échelle de nos besoins, notre objectif 2015 (70 000€ et 780 donateurs récurrents) représente le coût de 21 mètres d’autoroute, ou 0,0002% du C.A. annuel de Google</p>',
        'modale_footer' => '<a href="http://soutenir.framasoft.org" class="btn btn-soutenir"><i class="fa fa-fw fa-heart"></i> Soutenir</a>'
    ],
    'tip2014' => [
        'name'          => '35 000€ + 550 donateurs récurrents',
        'wkp'           => '',
        'description'   => 'Objectifs',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '3 permanents',
        'date_frama'    => '2014',
        'coordonnees'   => '',
        'class'         => 'objectifs',
        'soft_frama'    => '',
        'long_desc'     => 'Pour mener à bien ces projets, nous avons besoin de votre aide !',
        'url_frama'     => '',
        'id_frama'      => 'tip2014',
        'modale_title'  => 'Nos objectifs pour 2014',
        'modale_body'   => '<p>L’année 2014 aura été une année particulièrement chargée pour l’association Framasoft, puisqu’il aura fallu mettre au point notre plan de bataille pour vous permettre de résister à Google, Facebook et autres…</p>
        <p>L’association à notamment investi (en temps et en argent) dans l’infrastructure technique qui permet d’héberger les services actuels et nous permettront d’accueillir confortablement les applications que nous vous proposeront dans les prochaines années.</p>
        <p>Nous avons besoin de votre aide pour :</p>
        <ul>
            <li>accroître la taille de notre infrastructure technique (augmenter la puissance de calcul et le stockage de notre cluster de machines virtuelles)</li>
            <li>assurer les salaires de nos deux permanents</li>
            <li>permettre l’embauche d’un troisième salarié (chargé de communication)</li>
            <li>financer le développement d’améliorations de certains services proposés (Framadate et Framindmap, notamment)</li>
            <li>assurer la coordination de l’ensemble du projet "Dégooglisons Internet"</li>
        </ul>
        <p>L’objectif fixé est de passer de 400 à 550 donateurs récurrents, ainsi que de récolter 35 000€ en dons ponctuels sur l’année.</p>
        <p>Nous vous rappelons que Framasoft est une association largement autofinancée par ses donateurs. Pour vous donner une échelle de nos besoins, notre objectif 2014 (35 000€ et 550 donateurs récurrents) représente le coût de 8 mètres d’autoroute, ou 0,000083% du C.A. annuel de Google.</p>',
        'modale_footer' => '<a href="http://soutenir.framasoft.org" class="btn btn-soutenir"><i class="fa fa-fw fa-heart"></i> Soutenir</a>'
    ],
    'avaaz' => [                                                        // id="a-avaaz" (area), id="o-avaaz" (option), id="t-avaaz" (texte)
        'name'          => 'Avaaz',                                       // h2, alt, options
        'wkp'           => 'Avaaz.org',                                   // optionnel
        'description'   => 'Pétitions',
        'editeur'       => 'Avaaz',
        'wkp_editeur'   => 'Avaaz.org',                                   // optionnel
        'similaire'     => 'Change.org',
        'alt_online'    => '',
        'alt_offline'   => '<a href="https://github.com/WhiteHouse/petition">WeThePeople</a>, <a href="https://github.com/mysociety/petitions">MySociety</a>, <a href="https://www.drupal.org/project/webform">WebForm</a>…',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">pétition</b>',
        'date_frama'    => '2015',                                        // si chiffre → ( sortie prévue [2015](lien plm) )
        'coordonnees'   => '305,460,40',
        'class'         => 'potion',                                      // fight ou potion
        'soft_frama'    => 'Drupal + Webform',
        'long_desc'     => 'Lancez vos pétitions sans offrir les adresses de vos soutiens',
        'url_frama'     => '',
        'id_frama'      => 'framapetition',
        'modale_title'  => 'Libérez vos pétitions',
        'modale_body'   => '<p>
                                <b class="frama">Frama</b><b class="vert">pétition</b> vous permet de lancer des pétitions sur les sujets qui vous tiennent à cœur. En quelques clics, votre pétition est publiée et les adresses de vos soutiens ne seront pas exploités par des régies publicitaires.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> Inscrivez-vous, lancez une nouvelle pétition en suivant les instructions. Votre sondage sera créé. Vos soutiens n’aurons pas besoin de s’inscrire pour pouvoir y participer.
                            </p>',
        'modale_footer' => '<p class="precisions">Framapétition est une instance basée sur le logiciel libre <a href="https://www.drupal.org/">Drupal</a> et son module <a href="https://www.drupal.org/project/webform">WebForm</a></p>'
    ],
    'bitly' => [
        'name'          => 'Bit.ly',
        'wkp'           => 'Bit.ly',
        'description'   => 'Réduction d’URL',
        'editeur'       => 'Bitly',
        'wkp_editeur'   => 'Bit.ly',
        'similaire'     => 'goo.gl, t.co, tinyurl.com…',
        'alt_online'    => '<a href="http://lstu.fr/">lstu.fr</a>, <a href="http://ur1.ca/">ur1.ca</a>',
        'alt_offline'   => '<a href="http://lstu.fr/">LSTU</a>, <a href="http://yourls.org/">YOURLS</a>, <a href="http://lilurl.sourceforge.net/">LilURL</a>…',
        'alt_frama'     => '<a href="https://frama.link"><b class="violet">Frama</b>.<b class="vert">link</b></a> ou <a href="https://huit.re"><b class="violet">Huit</b>.<b class="vert">re</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/03/16/huit-re-framapic-framabin-framasoft-met-les-bouchees-triples/">mars 2015</a>)',
        'coordonnees'   => '233,535,40',
        'class'         => 'fight',
        'soft_frama'    => 'LSTU',
        'long_desc'     => 'Des adresses plus courtes en toute discrétion',
        'url_frama'     => 'https://frama.link',
        'id_frama'      => 'framashort',
        'modale_title'  => 'Finies les adresses trop longues',
        'modale_body'   => '<p>
                                Avec <a href="https://frama.link">Frama.link</a> ou <a href="https://huit.re">Huit.re</a>, il est possible de raccourcir des adresses web trop longues sans toutefois en tracer l’utilisation.
                                Sur les réseaux sociaux et le microblogging, disposer d’une adresse raccourcie permet d’économiser de l’espace et gagner en clarté.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> entrez l’adresse dans sa version originale et Frama.link la raccourcira.
                            </p>',
        'modale_footer' => '<p class="precisions">Frama.link est une instance basée sur <a href="http://lstu.fiat-tux.fr/"><abbr>LSTU</abbr></a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-lstu/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'blogger' => [
        'name'          => 'Blogger',
        'wkp'           => 'Blogger',
        'description'   => 'Hébergement de sites',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => 'sites.pages-jaunes.fr, OverBlog, Skyblog…',
        'alt_online'    => '<a href="http://wordpress.com">Wordpress.com</a>',
        'alt_offline'   => '<a href="http://www.pluxml.org/">PluXML</a>, <a href="http://wordpress.org">Wordpress</a>…',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">sites</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '575,400,40',
        'class'         => '',
        'soft_frama'    => 'Pluxml',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framasites',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framaxxxx est une instance basée sur <a href="http://www.pluxml.org/">Pluxml</a></p>'
    ],
    'bubblus' => [
        'name'          => 'Bubbl.us',
        'wkp'           => '',
        'description'   => 'Cartes heuristiques',
        'editeur'       => 'LKCollab',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '<a href="http://wisemapping.com/">Wisemapping.com</a>, <a href="https://www.mindmup.com">Mindmup</a>',
        'alt_offline'   => '<a href="http://wisemapping.com/">Wisemapping</a>, <a href="https://github.com/drichard/mindmaps">Mindmaps</a>, <a href="https://github.com/mindmup/mindmup">Mindmup</a>',
        'alt_frama'     => '<a href="http://framindmap.org"><b class="violet">Fram</b><b class="vert">indmap</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2012/10/16/framindmap-carte-mentale">octobre 2012</a>)',
        'coordonnees'   => '465,575,40',
        'class'         => 'fight',
        'soft_frama'    => 'Wisemapping',
        'long_desc'     => 'Fabriquez vos cartes mentales',
        'url_frama'     => 'http://framindmap.org',
        'id_frama'      => 'framindmap',
        'modale_title'  => 'Pourquoi créer une carte mentale ?',
        'modale_body'   => '<p>
                                <a href="http://framindmap.org/">Framindmap</a> vous permet d’organiser un brainstorming, ordonner vos idées à plusieurs, apprendre et faire apprendre une leçon, réaliser des classifications, identifier les éléments importants.
                            </p>
                               <p><video controls="controls" preload="none"
                                width="420" height="248" poster="http://www.framatube.org/images/media/866l.jpg">
                                <source src="http://www.framatube.org/files/1230-framindmap-creer-une-carte-mentale.mp4" type="video/mp4"></source>
                                <source src="http://www.framatube.org/files/1229-framindmap-creer-une-carte-mentale.webm" type="video/webm"></source>
                                  </video>
                               </p>
                               <p><small><em>Tutoriel réalisé par Claire Cassaigne</em> - La <a href="http://www.framatube.org/files/1229-framindmap-creer-une-carte-mentale.webm">vidéo</a> au format webm</small></p>

                            <p>
                                <b class="violet">Pourquoi utiliser Framindmap ?</b> Il est utilisable en ligne, sans installation, aucune inscription n’est requise, vous pouvez exporter
                                votre document sous forme d’image, c’est un logiciel libre et gratuit, les données vous appartiennent.
                            </p>
                            ',
        'modale_footer' => '<p class="precisions">Framindmap est une instance basée sur <a href="http://www.wisemapping.com/">Wisemapping</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-wisemapping/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'doodle' => [
        'name'          => 'Doodle',
        'wkp'           => 'Doodle.com',
        'description'   => 'Réunions et sondages',
        'editeur'       => 'Doodle AG',
        'wkp_editeur'   => 'Doodle.com',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="https://git.framasoft.org/framasoft/framadate">Framadate</a>, <a href="http://www.peacefrogs.net/papillon">Papillon</a>',
        'alt_frama'     => '<a href="http://framadate.org"><b class="violet">Frama</b><b class="vert">date</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2011/06/28/Planifier-vos-rendez-vous-avec-Framadate">juin 2011</a>)',
        'coordonnees'   => '605,295,40',
        'class'         => 'fight',
        'soft_frama'    => 'Studs',
        'long_desc'     => 'Convenir d’une réunion et créer un sondage',
        'url_frama'     => 'http://framadate.org',
        'id_frama'      => 'framadate',
        'modale_title'  => 'Organiser des rendez-vous simplement et librement',
        'modale_body'   => '<p>
                                Avec <a href="http://framadate.org">Framadate</a> vous serez en mesure de planifier un rendez-vous à plusieurs ou créer un sondage en ligne. Framadate ne stocke
                                pas vos données !
                            </p>
                            <p class="violet">Fonctionnalités :</p>
                            <ul>
                                <li>envoi de courriel à chaque réponse</li>
                                <li>modification du sondage en cours</li>
                                <li>planification d’une dead-line</li>
                                <li>nommez votre sondage et communiquez l’adresse à vos collaborateurs</li>
                            </ul>',
        'modale_footer' => '<p class="precisions">Framadate est une instance basée sur <a href="http://studs.u-strasbg.fr/">Studs</a> largement remaniée</p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-framadate/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'dropbox' => [
        'name'          => 'Dropbox',
        'wkp'           => 'Dropbox',
        'description'   => 'Stockage de documents',
        'editeur'       => 'Dropbox',
        'wkp_editeur'   => 'Dropbox',
        'similaire'     => 'Google Drive, iCloud Drive',
        'alt_online'    => '<a href="http://owncloud.org/providers/#free">OwnCloud</a>, <a href="https://seacloud.cc">Seacloud</a>',
        'alt_offline'   => '<a href="https://pyd.io/">Pyd.io</a>, <a href="http://owncloud.org/">OwnCloud</a>, <a href="http://www.seafile.com">Seafile</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">drive</b>',
        'date_frama'    => '2015',
        'coordonnees'   => '570,600,40',
        'class'         => 'potion',
        'soft_frama'    => 'Owncloud',
        'long_desc'     => 'Héberger ses documents en ligne',
        'url_frama'     => '',
        'id_frama'      => 'framadrive',
        'modale_title'  => 'Héberger ses documents en ligne',
        'modale_body'   => '
                            <p>
                                <b class="violet">Frama</b><b class="vert">drive</b> vous permet de stocker en ligne de manière chiffrée vos documents, de les synchroniser sur vos ordinateurs, tablettes, téléphones, etc et de les partager avec qui vous voulez…
                            </p>',
        'modale_footer' => '<p class="precisions">Framadrive est une instance basée sur <a href="http://owncloud/">Owncloud</a></p>'
    ],
    'dropsend' => [
        'name'          => 'Dropsend',
        'wkp'           => 'Dropsend',
        'description'   => 'Envoi de gros fichiers',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => 'Wetransfer',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">drop</b>',
        'date_frama'    => '2015',
        'coordonnees'   => '',
        'class'         => '',
        'soft_frama'    => 'LUFI',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framadrop',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framaxxxx est une instance basée sur <a href="######">Lufi</a></p>'
    ],
    'evernote' => [
        'name'          => 'Evernote',
        'wkp'           => 'Evernote',
        'description'   => 'Prise de notes',
        'editeur'       => 'Evernote',
        'wkp_editeur'   => 'Evernote',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="https://laverna.cc/">Laverna</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">notes</b>',
        'date_frama'    => '2017',
        'coordonnees'   => '590,480,40',
        'class'         => '',
        'soft_frama'    => 'Laverna',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framanotes',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framanote est une instance basée sur <a href="https://laverna.cc/">Laverna</a></p>'
    ],
    'facebook' => [
        'name'          => 'Facebook',
        'wkp'           => 'Facebook',
        'description'   => 'Réseau social',
        'editeur'       => 'Facebook',
        'wkp_editeur'   => 'Facebook',
        'similaire'     => 'LinkedIn, Viadeo, Google+',
        'alt_online'    => '<a href="http://pods.jasonrobinson.me/">liste de pods Diaspora*</a>, <a href="https://pod.movim.eu">liste de pods Movim</a>',
        'alt_offline'   => '<a href="https://diasporafoundation.org/">Diaspora*</a>, <a href="https://movim.eu/">Movim</a>',
        'alt_frama'     => '<a href="http://framasphere.org"><b class="violet">Frama</b><b class="vert">sphère</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2014/10/07/framasphere-reseau-social-libre-et-gratuit">octobre 2014</a>)',
        'coordonnees'   => '375,325,40',
        'class'         => 'fight',
        'soft_frama'    => 'Diaspora*',
        'long_desc'     => 'Un réseau social éthique et décentralisé',
        'url_frama'     => 'https://framasphere.org',
        'id_frama'      => 'framasphere',
        'modale_title'  => 'Rejoignez vos amis en zone libre',
        'modale_body'   => '<p>
                                Sur <a href="http://framasphere.org">Framasphere</a>, vous pouvez retrouver un réseau social qui respecte vos données. Framasphère est un nœud (appelé <i>pod</i>)
                                du réseau social libre Diaspora*.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> échanger des messages et photos avec n’importe quelle autre personne du réseau Diaspora*, gérer vos contacts, tags,
                                mentions, repartages… Vous pouvez aussi publier sur d’autres réseaux sociaux (Facebook, Twitter, Tumblr ou Wordpress).
                            </p>',
        'modale_footer' => '<p class="precisions">Framasphere est une instance basée sur <a href="https://diasporafoundation.org/">Diaspora*</a></p>'

    ],
    'github' => [
        'name'          => 'GitHub',
        'wkp'           => 'GitHub',
        'description'   => 'Hébergement de code',
        'editeur'       => 'GitHub',
        'wkp_editeur'   => 'GitHub',
        'similaire'     => 'Google Code, SourceForge',
        'alt_online'    => '<a href="http://savannah.gnu.org/">Savannah</a><a href="http://gna.org/">Gna!</a>, <a href="http://tuxfamily.org/">TuxFamilly</a>, l’<a href="https://adullact.net/">Adullact</a>, <a href="http://gitlab.org">Gitlab</a>',
        'alt_offline'   => '<a href="http://gitlab.org">Gitlab</a>',
        'alt_frama'     => '<a href="http://git.framasoft.org"><b class="violet">Frama</b><b class="vert">git</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/03/13/google-code-ferme-ses-portes-nous-on-les-ouvre/">mars 2015</a>)',
        'coordonnees'   => '320,565,40',
        'class'         => 'fight',
        'soft_frama'    => 'Gitlab',
        'long_desc'     => 'Du code libre c’est bien. Sur une plateforme libre c’est mieux !',
        'url_frama'     => 'http://git.framasoft.org',
        'id_frama'      => 'framagit',
        'modale_title'  => 'Hébergement de code libre',
        'modale_body'   => '<p>
                                <a href="http://git.framasoft.org">Framagit</a> est un outil qui s’adresse avant tout aux développeurs. Il permet la création de 42 dépôts maximum par compte (si vous avez besoin de plus, songez sérieusement à vous auto-héberger). Et, petit plus par rapport à GitHub, vous pouvez créer des dépôts privés.</p>
                            </p>
                            <p>Il est également possible de « mirrorer » automatiquement vos dépôts sur GitHub : vous continuez à « engraisser la bête », mais vous êtes déjà moins dépendant, et vous conservez une visibilité auprès des presque 10 millions d’inscrits sur GitHub. Votre dépôt sur notre Framagit est automatiquement poussé sur votre dépôt Github. C’est d’ailleurs la solution retenue par Framasoft, qui dispose toujours d’un compte GitHub, alors que les développements sont réalisés sur notre forge.
                            </p>
                            ',
        'modale_footer' => '<p class="precisions">Framagit est une instance basée sur <a href="https://about.gitlab.com/">Gitlab</a></p>'
    ],
    'gagenda' => [
        'name'          => 'Google Agenda',
        'wkp'           => 'Google_Agenda',
        'description'   => 'Agenda partagé',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://www.k5n.us/webcalendar.php">webcalendar</a>, <a href="http://www.zkoss.org/product/">ZK Calendar</a>…',
        'alt_frama'     => '<b class="violet">Fram</b><b class="vert">agenda</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '400,520,40',
        'class'         => '',
        'soft_frama'    => 'Webcalendar',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framagenda',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framagenda est une instance basée sur <a href="http://www.k5n.us/webcalendar.php">Webcalendar</a></p>'
    ],
    'gbooks' => [
        'name'          => 'Google Books',
        'wkp'           => 'Google_Livres',
        'description'   => 'Livres en ligne',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '<a href="http://fr.wikisource.org/wiki/Wikisource:Accueil">WikiSource</a>, <a href="http://gallica.bnf.fr/html/livres/livres">Gallica</a>…',
        'alt_offline'   => '<a href="https://github.com/rvolz/BicBucStriim">BicBucStriim</a>, <a href="https://github.com/seblucas/cops">Cops</a>, <a href="http://calibre2opds.com/">Calibre2OPDS</a>',
        'alt_frama'     => '<a href="http://framabookin.org"><b class="violet">Frama</b><b class="rouge">bookin</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/06/22/framabookin-devenez-le-concurrent-damazon">juin 2015</a>)',
        'coordonnees'   => '630,205,40',
        'class'         => 'fight',
        'soft_frama'    => 'BicBucStriim',
        'long_desc'     => 'Une bibliothèque de plusieurs milliers d’ouvrages',
        'url_frama'     => 'http://framabookin.org',
        'id_frama'      => 'framaopds',
        'modale_title'  => 'Catalogue d’ouvrages accessibles pour tous',
        'modale_body'   => '<p>
                                <b class="violet">Frama</b><b class="rouge">bookin</b> est le catalogue <abbr>OPDS</abbr> de Framasoft. « Euh… Oui, mais encore ? », nous direz-vous. Un catalogue <abbr>OPDS</abbr> est un site auquel vous pouvez vous abonner avec un logiciel supportant cette fonctionnalité (certaines liseuses en embarquent un, pour Android, il existe Aldiko et bien d’autres), vous donnant accès rapidement et simplement à tout une bibliothèque depuis votre ordinateur, smartphone ou tablette.
                            </p>
                            <p>Le catalogue <abbr>OPDS</abbr> de Framasoft vous donne accès à des centaines d’ouvrages non protégés par « copyright ».</p>',
        'modale_footer' => '<p class="precisions">Framabookin fournit un catalogue généré par les logiciels <a href="http://calibre-ebook.com/">Calibre</a> et <a href="https://github.com/rvolz/BicBucStriim">BicBucStriim</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-d-un-serveur-opds/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'gdocs' => [
        'name'          => 'Google Docs',
        'wkp'           => 'Google_Drive',
        'description'   => 'Rédaction collaborative',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://etherpad.org/">Etherpad</a>',
        'alt_frama'     => '<a href="https://framapad.org"><b class="violet">Frama</b><b class="vert">pad</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2011/03/28/framapad-collaboration-en-ligne">mars 2011</a>)',
        'coordonnees'   => '370,65,40',
        'class'         => 'fight',
        'soft_frama'    => 'Etherpad',
        'long_desc'     => 'Du traitement de texte en ligne et à plusieurs !',
        'url_frama'     => 'https://framapad.org',
        'id_frama'      => 'framapad',
        'modale_title'  => 'Outil de rédaction collaborative',
        'modale_body'   => '<p>
                                <a href="https://framapad.org">Framapad</a> est un service de rédaction collaborative en ligne basé sur le logiciel
                                <a href="http://etherpad.org/">Etherpad</a>.
                            </p>
                            <p>
                                Un « pad » est un éditeur de texte en ligne. Son vrai plus ? L’édition collaborative : les contributions de chaque
                                utilisateur apparaissent immédiatement dans les pads de tous les participants, signalées par un code couleur.
                            </p>
                            <p class="violet">Fonctionnalités :</p>
                            <ul>
                                <li>Ouvrir un pad public ou privé</li>
                                <li>Rédiger votre document</li>
                                <li>Inviter des collaborateurs</li>
                                <li>Un code couleur pour chacun</li>
                                <li>Tchat intégré</li>
                                <li>Historique des versions</li>
                                <li>Exporter votre travail</li>
                                <li>Et bientôt : gérer des sessions et des groupes avec MyPads !</li>
                            </ul>',
        'modale_footer' => '<p class="precisions">Framapad est une instance basée sur <a href="http://etherpad.org/">Etherpad</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-detherpad/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'

    ],
    'gforms' => [
        'name'          => 'Google Forms',
        'wkp'           => '',
        'description'   => 'Questionnaires en ligne',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="https://github.com/spiral-project/daybed">DayBed</a>, <a href="https://www.drupal.org/project/webform">WebForm</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">forms</b>',
        'date_frama'    => '2017',
        'coordonnees'   => '500,380,40',
        'class'         => 'potion',
        'soft_frama'    => 'Drupal + Webform',
        'long_desc'     => 'Créez simplement des questionnaires',
        'url_frama'     => '',
        'id_frama'      => 'framaforms',
        'modale_title'  => 'Création de questionaires',
        'modale_body'   => '<p>
                                <b class="violet">Frama</b><b class="vert">forms</b> vous permet de créer les questionaires dont vous avez besoin, que ce soit dans un cadre scolaire, familial ou associatif. Créez vos questions en fonction de vos besoins, des formulaires à choix multiples aux champs de texte libre.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement</b> : créez un compte sur Framaforms et lancez-vous dans la création des questionnaires, en fonction de vos besoins. Vos sondés n’ont pas besoin de s’inscrire pour pouvoir répondre. Vous disposez d’une synthèse des réponses.
                            </p>',
        'modale_footer' => '<p class="precisions">Framaforms est une instance basée sur <a href="https://www.drupal.org/">Drupal</a> et son module <a href="https://www.drupal.org/project/webform">Webform</a></p>'
    ],
    'ggroups' => [
        'name'          => 'Google Groupes',
        'wkp'           => 'Google_Groupes',
        'description'   => 'Listes de diffusion',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '<a href="http://riseup.net/">riseup.net</a>',
        'alt_offline'   => '<a href="http://www.gnu.org/software/mailman/">mailman</a>, <a href="http://www.sympa.org/">sympa</a>…',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">listes</b>',
        'date_frama'    => '2017',
        'coordonnees'   => '615,725,40',
        'class'         => '',
        'soft_frama'    => 'Sympa',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framalistes',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framalistes est une instance basée sur <a href="http://www.sympa.org/">Sympa</a></p>'
    ],
    'greader' => [
        'name'          => 'Google Reader',
        'wkp'           => 'Google_Reader',
        'description'   => 'Lecteur de flux',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => 'Feedly',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://tt-rss.org/">TinyTinyRSS</a>, <a href="http://freshrss.org/">FreshRSS</a>…',
        'alt_frama'     => '<a href="https://framanews.org"><b class="violet">Frama</b><b class="vert">news</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2013/06/27/framanews-rss-google-reader">juin 2013</a>)',
        'coordonnees'   => '190,610,40',
        'class'         => 'fight',
        'soft_frama'    => 'TinyTinyRSS',
        'long_desc'     => 'Suivez l’actualité et faites vos choix librement',
        'url_frama'     => 'https://framanews.org',
        'id_frama'      => 'framanews',
        'modale_title'  => 'Toujours au courant, jamais en retard',
        'modale_body'   => '<p>
                                <a href="http://framanews.org">Framanews</a> vous propose un lecteur de flux RSS en ligne, vous permettant d’être toujours au courant de l’actualité à partir des flux RSS de vos sites préférés !
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> après avoir créé votre compte, enregistrez vos flux RSS et suivez-les.
                                Comment faire plus simple ?
                            </p>',
        'modale_footer' => '<p class="precisions">Framanews est une instance basée sur <a href="http://tt-rss.org/redmine/projects/tt-rss/wiki">tt-rss</a></p>'

    ],
    'gsearch' => [
        'name'          => 'Google Search',
        'wkp'           => 'Google_%28moteur_de_recherche%29',
        'description'   => 'Moteur de recherche',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '<a href="https://duckduckgo.com/">DuckDuckGo</a>, <a href="https://www.ixquick.com/">Ixquick</a>',
        'alt_offline'   => '<a href="https://github.com/asciimoo/searx">Searx</a>, <a href="http://codingteam.net/project/mysearch">MySearch</a>, <a href="http://fr.wikipedia.org/wiki/YaCy">YaCy</a>, <a href="http://fr.wikipedia.org/wiki/Seeks">Seeks</a>',
        'alt_frama'     => '<a href="https://framabee.org"><b class="violet">Frama</b><b class="vert">bee</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/05/05/framabee-le-meta-moteur-qui-va-vous-butiner-le-web/">mai 2015</a>)',
        'coordonnees'   => '350,245,40',
        'class'         => 'fight',
        'soft_frama'    => 'Searx',
        'long_desc'     => 'Et si vos recherches n’étaient plus fichées par Google ?',
        'url_frama'     => 'https://framabee.org',
        'id_frama'      => 'framabee',
        'modale_title'  => 'Une recherche anonyme',
        'modale_body'   => '<p>
                                <a href="https://framabee.org/"><b class="violet">Frama</b><b class="vert">bee</b></a> est un meta-moteur de recherche anonyme. Cela signifie que vos recherches effectuées au travers de ce moteur sont envoyées à différents moteurs, avant d’être affichées dans votre navigateur. L’intérêt, c’est que c’est notre moteur qui fait alors office d’intermédiaire entre vous et Google (ou les autres moteurs). Votre adresse IP n’est donc pas enregistrée chez eux. </p>
                                <p>Evidemment, vous devez avoir confiance en Framasoft/Framabee si vous souhaitez l’utiliser, c’est pourquoi nous proposons une <a href="/nav/html/charte.html">charte</a>.
                            </p>',
        'modale_footer' => '<p class="precisions">Framabee est une instance basée sur <a href="https://searx.0x2a.tk/">Searx</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-searx/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'gslides' => [
        'name'          => 'Google Slides',
        'wkp'           => 'Google_Drive',
        'description'   => 'Présentations',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '<a href="http://strut.io/">Strut.io</a>',
        'alt_offline'   => '<a href="http://strut.io/">Strut.io</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">slides</b>',
        'date_frama'    => '2015',
        'coordonnees'   => '635,555,40',
        'class'         => 'potion',
        'soft_frama'    => 'Strut.io',
        'long_desc'     => 'Des diaporamas de haute qualité sans se fatiguer',
        'url_frama'     => '',
        'id_frama'      => 'framimpress',
        'modale_title'  => 'Une application pour vos diaporamas',
        'modale_body'   => '<p>
                                Framimpress vous permettra
                                de réaliser facilement et intuitivement vos diaporamas.
                                Grâce à cette application dans votre navigateur, nul besoin
                                de charger des logiciels lourds et compliqués.
                            </p>
                            <p class="violet">Fonctionnalités :</p>
                            <ul>
                                <li>une interface claire et intuitive &mdash; insertion facile de texte, d’images, de vidéos et de site web</li>
                                <li>sauvegarde en local de la version HTML prête à l’emploi.</li>
                            </ul>
                            <p><b class="violet">Pour plus tard,</b> si nos finances le permettent :</p>
                            <ul>
                                <li>la sauvegarde dans les nuages dans une boîte privée</li>
                                <li>une fonction d’édition collaborative…</li>
                            </ul>',
        'modale_footer' => '<p class="precisions">Framimpress est une instance basée sur <a href="http://strut.io/">Strut.io</a></p>'

    ],
    'gspreadsheet' => [
        'name'          => 'Google Spreadsheet',
        'wkp'           => 'Google_Drive',
        'description'   => 'Tableur collaboratif',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="https://ethercalc.org/">Ethercalc</a>, <a href="http://www.zkoss.org/product/zkspreadsheet">ZK Spreadsheet</a>',
        'alt_frama'     => '<a href="https://framacalc.org"><b class="violet">Frama</b><b class="vert">calc</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2012/10/03/framacalc-tableur-sur-internet">octobre 2012</a>)',
        'coordonnees'   => '375,660,40',
        'class'         => 'fight',
        'soft_frama'    => 'Ethercalc',
        'long_desc'     => 'Partagez vos tableaux et collaborez !',
        'url_frama'     => 'http://framacalc.org',
        'id_frama'      => 'framacalc',
        'modale_title'  => 'Un tableur en ligne',
        'modale_body'   => '<p>
                                Avec <a href="https://framacalc.org">Framacalc</a>, vos données sont automatiquement sauvegardées sur Internet. Vous et vos collaborateurs pouvez collaborer sur
                                le document en même temps. Visualisez tous les changements en temps réel et travaillez ensemble sur vos inventaires, vos calculs, vos données, vos statistiques,
                                et bien plus !
                            </p>
                            <p class="violet">Fonctionnalités :</p>
                            <ul>
                                <li>Édition collaborative (plusieurs utilisateurs connectés à la même feuille de calcul)</li>
                                <li>Nombreuses fonctions disponibles (statistiques, financières, mathématiques, texte, etc.)</li>
                                <li>Possibilité de commenter des cellules</li>
                                <li>Sauvegarde automatique</li>
                                <li>Graphiques de base (histogramme, lignes, points)</li>
                                <li>Export HTML</li>
                                <li>Taille du document : jusqu’à 100 000 lignes.</li>
                            </ul>',
        'modale_footer' => '<p class="precisions">Framacalc est une instance basée sur <a href="https://www.ethercalc.org/">Ethercalc</a></p>'

    ],
    'imgur' => [
        'name'          => 'Img.ur',
        'wkp'           => 'Imgur',
        'description'   => 'Envoi d’images',
        'editeur'       => 'Imgur',
        'wkp_editeur'   => 'Imgur',
        'similaire'     => '',
        'alt_online'    => '<a href="http://pix.toile-libre.org/">Toile libre</a>, <a href="https://lut.im">Lut.im</a>',
        'alt_offline'   => '<a href="https://lut.im">Lut.im</a>, <a href="https://coquelicot.potager.org/">Coquelicot</a>',
        'alt_frama'     => '<a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/03/16/huit-re-framapic-framabin-framasoft-met-les-bouchees-triples/">mars 2015</a>)',
        'coordonnees'   => '430,90,40',
        'class'         => 'fight',
        'soft_frama'    => 'Lut.im',
        'long_desc'     => 'Partagez vos images anonymement',
        'url_frama'     => 'https://framapic.org',
        'id_frama'      => 'framalutim',
        'modale_title'  => 'Partagez vos images facilement',
        'modale_body'   => '<p>
                                <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a> vous permet de partager facilement des images, par exemple pour les partager sur Twitter, Facebook, ou … Framasphère !
                            </p>
                            <p>Attention, ce service n’est pas un équivalent de flickr ou instagram : il ne s’agit pas d’un service d’hébergement de photos d’utilisateurs, mais bien d’un service d’hébergement d’images anonymes (même nous, nous n’avons pas accès au contenu, qui est chiffré).</p>
                            <p>
                                <b class="violet">Frama</b><b class="vert">pic</b> permet d’envoyer des images de façon anonyme. Vous pouvez décider du moment de leur suppression (dès la première visualisation, 24H/7j/30j/1an après leur mise en ligne).
                            </p>',
        'modale_footer' => '<p class="precisions">Framapic est une instance basée sur <a href="https://lut.im/">Lut.im</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-lutim/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'jsfiddle' => [
        'name'          => 'jsFiddle',
        'wkp'           => '',
        'description'   => 'Partage de code',
        'editeur'       => 'jsFiddle',
        'wkp_editeur'   => '',
        'similaire'     => 'Codepen',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://jsbin.com/">JS Bin</a>, <a href="http://dabblet.com/">Dabblet</a>…',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">xxx</b>',
        'date_frama'    => '2017',
        'coordonnees'   => '530,300,40',
        'class'         => '',
        'soft_frama'    => 'jsbin',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framajscode',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framaxxxx est une instance basée sur <a href="http://jsbin.com">jsbin</a></p>'
    ],
    'kanban' => [
        'name'          => 'Trello',
        'wkp'           => 'Trello',
        'description'   => 'Gestion de tâches',
        'editeur'       => 'Trello Inc',
        'wkp_editeur'   => 'Trello Inc',
        'similaire'     => '',
        'alt_online'    => 'Libreboard',
        'alt_offline'   => '',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">board</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '',
        'class'         => 'potion',
        'soft_frama'    => 'Kanboard',
        'long_desc'     => 'Gestionnaire de projets',
        'url_frama'     => '',
        'id_frama'      => 'framaboard',
        'modale_title'  => 'Gestionnaire de projets',
        'modale_body'   => '<p>
                                <b class="violet">Frama</b><b class="vert">board</b> est un gétionnaire de tâches visuel. Il permet de gérer des projets de manière collaborative, en suivant la méthode Kanban. Son système visuel permet de s’y retrouver au premier coup d’œil, quelque soit votre habitude à utiliser ce genre d’outil.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> Lancez votre projet, et commencez à le gérer collectivement, tâche par tâche. Assignez une personne à chaque tache, définissez des fonctions, des avancements et des délais. Cette gestion visuelle vous permettra de voir l’état du projet en un coup d’œil.
                            </p>',
        'modale_footer' => '<p class="precisions">Framaboard est une instance basée sur <a href="http://kanboard.net/">Kanboard</a></p>'
    ],
    'padlet' => [
        'name'          => 'Padlet',
        'wkp'           => '',
        'description'   => 'Organisation d’idées',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">xxx</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '',
        'class'         => '',
        'soft_frama'    => 'Scrumblr',
        'long_desc'     => 'xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx',
        'url_frama'     => '',
        'id_frama'      => 'framascrumblr',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framaxxxx est une instance basée sur <a href="http://scrumblr.ca/">Scrumblr</a></p>'
    ],
    'pastebin' => [
        'name'          => 'Pastebin',
        'wkp'           => 'Pastebin',
        'description'   => 'Notes anonymes',
        'editeur'       => 'Pastebin',
        'wkp_editeur'   => 'Pastebin',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://sebsauvage.net/wiki/doku.php?id=php:zerobin">Zérobin</a>',
        'alt_frama'     => '<a href="https://framabin.org"><b class="violet">Frama</b><b class="vert">bin</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/03/16/huit-re-framapic-framabin-framasoft-met-les-bouchees-triples/">mars 2015</a>)',
        'coordonnees'   => '360,150,40',
        'class'         => 'fight',
        'soft_frama'    => 'Zerobin',
        'long_desc'     => 'Rédigez et partagez en toute discrétion',
        'url_frama'     => 'https://framabin.org',
        'id_frama'      => 'framabin',
        'modale_title'  => 'Communiquez des données chiffrées',
        'modale_body'   => '<p>
                                Avec <a href="https://framapin.org">Framabin</a> vous pouvez partager des informations dont seuls vous et votre correspondant aurez le pouvoir de déchiffrer, et surtout de
                                manière très simple. Vous pouvez même choisir la durée de validité de l’accès !
                            </p>
                            <p>
                                <b class="violet">Pour quels usages ?</b> Vous pouvez avoir besoin de communiquer une ou plusieurs informations sans vouloir passer par la voie classique
                                du courriel ou du petit bout de papier. Framabin vous permet d’envoyer une adresse et donner accès à ces informations en toute sécurité et simplicité. Le
                                serveur stocke des données illisibles pour lui.
                            </p>',
        'modale_footer' => '<p class="precisions">Framabin est une instance basée sur <a href="http://sebsauvage.net/wiki/doku.php?id=php:zerobin">Zerobin</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-zerobin/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'pocket' => [
        'name'          => 'Pocket',
        'wkp'           => 'Pocket_%28application%29',
        'description'   => 'Sauvegarde de contenu',
        'editeur'       => 'Read It Later',
        'wkp_editeur'   => 'Pocket_%28application%29',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="https://www.wallabag.org/">Wallabag</a>',
        'alt_frama'     => '<a href="https://framabag.org"><b class="violet">Frama</b><b class="vert">bag</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2014/02/05/Framabag-service-libre-gratuit-interview-developpeur">février 2014</a>)',
        'coordonnees'   => '280,630,40',
        'class'         => 'fight',
        'soft_frama'    => 'Wallabag',
        'long_desc'     => 'Sauvegardez et lisez plus tard',
        'url_frama'     => 'http://framabag.org',
        'id_frama'      => 'framabag',
        'modale_title'  => 'Sauvegardez, lisez plus tard',
        'modale_body'   => '<p>
                                Avec <a href="https://www.framabag.org/">Framabag</a>, vous ne perdrez plus les contenus du Web qui vous intéressent mais que vous n’avez pas le temps de
                                parcourir. D’un clic, vous enregistrez votre sélection et vous la lirez quand vous voudrez. L’application sauvegarde votre sélection pour vous permettre d’en
                                profiter quand vous en aurez le temps.
                            </p>
                            <p>
                                Framabag est un <b class="violet">service de sauvegarde de pages web</b>. Vous stockez sur le serveur les contenus qui vous intéressent, textes et images
                                comprises. Vous pouvez aussi partager vos articles et utiliser des extensions pour Firefox, Chrome, Android…
                            </p>',
        'modale_footer' => '<p class="precisions">Framabag est une instance basée sur <a href="https://www.wallabag.org/">Wallabag</a></p>'
    ],
    'scribd' => [
        'name'          => 'Scribd',
        'wkp'           => '',
        'description'   => 'Partage de PDF/ODP',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">xxx</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '',
        'class'         => '',
        'soft_frama'    => 'WebODF, PDFy',
        'long_desc'     => '',
        'url_frama'     => '',
        'id_frama'      => 'framaopdf',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framaxxx est une instance basée sur <a href="http://webodf.org/">WebODF</a> ou <a href="https://pdf.yt/">PDFy</a></p>'
    ],
    'skype' => [
        'name'          => 'Skype',
        'wkp'           => 'Skype',
        'description'   => 'Visioconférence',
        'editeur'       => 'Microsoft',
        'wkp_editeur'   => 'Microsoft',
        'similaire'     => 'Google Hangouts',
        'alt_online'    => '<a href="http://hibuddy.monkeypatch.me/">Hi Buddy</a> (WebRTC)',
        'alt_offline'   => '<a href="https://jitsi.org/">Jitsi</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">talk</b>',
        'date_frama'    => '2015',
        'coordonnees'   => '230,460,40',
        'class'         => 'potion',
        'soft_frama'    => 'Jitsi Meet',
        'long_desc'     => 'Discutez librement avec vos amis',
        'url_frama'     => '',
        'id_frama'      => 'framatalk',
        'modale_title'  => 'Vidéoconférence',
        'modale_body'   => '<p>
                                <b class="violet">Frama</b><b class="vert">talk</b> vous permet de discuter simplement avec vos amis, sans installation complexe de logiciel. Vous discutez simplement, sans aucune analyse des données liées aux conversations. Tout se passe entre vous et vos amis.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> Directement à partir de votre navigateur web, vous êtes mis en relation avec vos amis, sans devoir installer de logiciel supplémentaire. Une fois connecté, donnez l’autorisation à votre navigateur d’utiliser votre micro et votre webcam, et profitez de la discussion en toute simplicité.
                            </p>',
        'modale_footer' => '<p class="precisions">Framatalk est une instance basée sur <a href="https://jitsi.org/Projects/JitsiMeet">Jitsi Meet</a></p>'
    ],
    'twitter' => [
        'name'          => 'Twitter',
        'wkp'           => 'Twitter',
        'description'   => 'Microblogging',
        'editeur'       => 'Twitter',
        'wkp_editeur'   => 'Twitter',
        'similaire'     => '',
        'alt_online'    => '<a href="https://identi.ca">Identi.ca</a>',
        'alt_offline'   => '<a href="http://twister.net.co/">Twister</a>, <a href="http://status.net/">StatusNet</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">tweet</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '415,205,40',
        'class'         => '',
        'soft_frama'    => 'Twister',
        'long_desc'     => 'Gazouillez sur Internet',
        'url_frama'     => '',
        'id_frama'      => 'framatweet',
        'modale_title'  => 'Le microblogage libre',
        'modale_body'   => '<p>
                                <a href="###">Framatweet</a> vous permet de suivre des flux de microblogage et entretenir vos propres flux de manière décentralisée en utilisant le protocole P2P.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> utilisez un client de connexion, ouvrez un compte, abonnez-vous à des flux et tweetez à votre tour.
                            </p>',
        'modale_footer' => '<p class="precisions">Framatweet est une instance basée sur <a href="http://twister.net.co/">Twister</a></p>'
    ],
/*    'youporn' => [
        'name'          => 'Youporn',
        'wkp'           => 'Youporn',
        'description'   => 'Les codes du porno',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">porn</b>',
        'date_frama'    => '2015',
        'coordonnees'   => '',
        'class'         => '',
        'soft_frama'    => 'GermanPorn',
        'long_desc'     => '',
        'url_frama'     => '',
        'id_frama'      => 'framaporn',
        'modale_title'  => 'xxxxxxxx xxxxxxx xxxxxxx',
        'modale_body'   => '<p>
                                <a href="###">Framaxxx</a> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx
                                xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>
                            <p>
                                <b class="violet">xxxxx xxxxxxxx xxxx</b> xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxxxxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                                xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx xxxxxxxxxxxxx xxxxxx xxxxxxxxx xxxxxx
                            </p>',
        'modale_footer' => '<p class="precisions">Framaxxx est une instance basée sur <a href="#######">xxxxxx</a></p>'
    ],*/
    'youtube' => [
        'name'          => 'Youtube',
        'wkp'           => 'Youtube',
        'description'   => 'Hébergement de vidéos',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => 'Vimeo, Dailymotion…',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://mediagoblin.org/">Mediagoblin</a>, <a href="http://cumulusclips.org/">CumulusClips</a>,<a href="http://www.mediaspip.net/">MediaSpip</a>, <a href="http://www.kaltura.org/">Kaltura</a>, <a href="http://plumi.org/">Plumi</a>',
        'alt_frama'     => '<a href="http://framatube.org"><b class="violet">Frama</b><b class="rouge">tube</b></a>',
        'date_frama'    => '2016',
        'coordonnees'   => '500,480,40',
        'class'         => 'potion',
        'soft_frama'    => 'Mediagoblin',
        'long_desc'     => 'Partagez vos vidéos et conservez vos droits',
        'url_frama'     => 'http://framatube.org',
        'id_frama'      => 'framatube',
        'modale_title'  => 'Libérez vos vidéos',
        'modale_body'   => '<p>
                                Sur <a href="http://framatube.org">Framatube</a>, vous pouvez télécharger vos vidéos, les partager avec votre entourage ou les ouvrir en lecture publique.
                                C’est le meilleur moyen de partager des vidéos sans céder vos droits.
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> ouvrez un compte et téléchargez vos vidéos. Vous gardez vos droits et la liberté de modifier vos téléchargements.
                            </p>',
        'modale_footer' => '<p class="precisions">Framatube est une instance basée sur <a href="http://mediagoblin.org/">Mediagoblin</a></p>'
    ],
    // Pas de concurrent
    'framagames' => [
        'name'          => 'Angry birds',
        'wkp'           => '',
        'description'   => 'Jeux',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<a href="http://framagames.org"><b class="violet">Frama</b><b class="vert">games</b></a>',
        'date_frama'    => '(depuis <a href="http://framablog.org/2015/06/18/framagames-des-jeux-pour-changer-les-idees-aux-lyceens/">juin 2015</a>)',
        'coordonnees'   => '',
        'class'         => 'fight',
        'soft_frama'    => 'divers',
        'long_desc'     => 'Une compilation de jeux libre',
        'url_frama'     => 'http://framagames.org',
        'id_frama'      => 'framagames',
        'modale_title'  => 'Faites une pose, jouez quelques instants',
        'modale_body'   => '<p>
                                <a href="http://framagames.org"><b class="violet">Frama</b><b class="vert">games</b></a>, vous propose un certain nombre de jeux libres, disponibles aussi bien pour une utilisation en ligne qu’hors ligne. Faites une pause et profitez de l’un des jeux proposés.
                                <small>(et <a href="https://fr.wikipedia.org/wiki/Angry_Birds#Collecte_de_donn.C3.A9es_par_la_NSA">contrairement à Angry Birds</a>, la NSA ne vous espionnera pas ;) )</small>
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> Chacun des jeux est directement utilisable sur le site ou téléchargeable pour une utilisation hors ligne. Sélectionnez votre jeu et lancez-vous.
                            </p>',
'modale_footer' => '<p class="precisions">Framagames est une compilation de jeux libres. Les crédits sont indiqués sur le site.</p>'
    ],
    'framavectoriel' => [
        'name'          => 'Pixlr',
        'wkp'           => '',
        'description'   => 'Dessin vectoriel',
        'editeur'       => 'Autodesk',
        'wkp_editeur'   => 'Autodesk',
        'similaire'     => 'Picozu',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<a href="http://framavectoriel.org"><b class="violet">Frama</b><b class="vert">vectoriel</b></a>',
        'date_frama'    => '(depuis <a href="http://www.framablog.org/index.php/post/2012/10/23/framavectoriel">octobre 2012</a>)',
        'coordonnees'   => '',
        'class'         => 'fight',
        'soft_frama'    => 'SVG-Edit',
        'long_desc'     => 'Créez rapidement des images vectorielles au format SVG',
        'url_frama'     => 'http://framavectoriel.org',
        'id_frama'      => 'framavectoriel',
        'modale_title'  => 'Créez rapidement des images vectorielles au format SVG',
        'modale_body'   => '<p>
                                <a href="http://framavectoriel.org"><b class="violet">Frama</b><b class="vert">vectoriel</b></a>, est un logiciel de dessin extrêmement simple (qui a dit simpliste ?).
                            </p>
                            <p>
                                <b class="violet">Fonctionnalité :</b>

                                    <ul>
                                        <li>Utilisable en ligne, sans installation</li>
                                        <li>Pas d’inscription requise</li>
                                        <li>Possibilité d’exporter votre document sous forme d’image bitmap ou vectorielles</li>
                                        <li>Logiciel libre et gratuit, ouvert à tous</li>
                                        <li>Ethique : aucune conservation de vos données</li>

                                    </ul>
                                    <div style="text-align:center">
                                    <video controls="controls" preload="none" width="620" height="360" poster="http://www.framatube.org/images/media/864l.jpg">
                                        <source src="http://www.framatube.org/files/1227-introduction-a-svg-editjpg.mp4" type="video/mp4"></source>
                                        <source src="http://www.framatube.org/files/1228-introduction-a-svg-editjpg.webm" type="video/webm"></source>
                                    </video>
                                    </div>
                                    <p> La <a href="http://www.framatube.org/files/1228-introduction-a-svg-editjpg.webm">vidéo</a> au format webm <small>(Pour l’instant la vidéo est en anglais. Désolé.)</small></p>
                            </p>',
        'modale_footer' => '<p class="precisions">Framavectoriel est une instance basée sur <a href="https://code.google.com/p/svg-edit/">SVG-Edit</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-svg-edit/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'loomio' => [
        'name'          => '',
        'wkp'           => '',
        'description'   => 'Prise de décision',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">xxx</b>',
        'date_frama'    => '2016',
        'coordonnees'   => '',
        'class'         => 'potion',
        'soft_frama'    => 'loomio',
        'long_desc'     => 'Un outil pour mieux gérer les prises de décisions collectives',
        'url_frama'     => '',
        'id_frama'      => 'framaloomio',
        'modale_title'  => 'Décidez ensembles,',
        'modale_body'   => '<p>
                                Framaloomio vous aide à prendre des décisions communautaires. Discutez, échangez, mettez-vous d’accord et passez à l’action, le tout dans les délais que vous fixés au départ.
                            </p>
                            <p>
                                 <b class="violet">Fonctionnement :</b> Lancez une discussion, et invitez les personnes concernées à venir apporter leur point de vue. Développez les idées, échangez quelque soit votre point de vue. Les avis peuvent tous s’exprimer, se partager et murir. Décidez ensemble et sortez une proposition finale, dans les délais définis.
                            </p >',
        'modale_footer' => '<p class="precisions">Framaxxx est une instance basée sur <a href="https://www.loomio.org">loomio</a></p>'
    ],
    // Mises à jour
    'framadate2' => [
        'name'          => 'Développement axé sur l’ergonomie et l’accessibilité',
        'wkp'           => '',
        'description'   => 'Réunions et sondages',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<a href="http://framadate.org"><b class="violet">Frama</b><b class="vert">date</b></a>',
        'date_frama'    => '2014',
        'coordonnees'   => '',
        'class'         => 'casque',
        'soft_frama'    => 'Studs',
        'long_desc'     => 'Convenir d’une réunion et créer un sondage',
        'url_frama'     => 'http://framadate.org',
        'id_frama'      => 'framadate2',
        'modale_title'  => 'Organiser des rendez-vous simplement et librement',
        'modale_body'   => '<p>
                                Avec <a href="http://framadate.org">Framadate</a> vous serez en mesure de planifier un rendez-vous à plusieurs ou créer un sondage en ligne. Framadate ne stocke
                                pas vos données !
                            </p>
                            <p class="violet"><b>Fonctionnalités :</b></p>
                            <ul>
                                <li>envoi de courriel à chaque réponse</li>
                                <li>modification du sondage en cours</li>
                                <li>planification d’une dead-line</li>
                                <li>nommez votre sondage et communiquez l’adresse à vos collaborateurs</li>
                            </ul>
                            <p>
                                <b class="violet">Mise à jour</b> : la mise à jour de Framadate concerne une refonte graphique et technique du service. L’interface est plus claire, plus ergonomique. Et il sera désormais possible de répondre "peut-être" à un choix de sondage.
                            </p>',
        'modale_footer' => '<p class="precisions">Framadate est une instance basée sur <a href="http://studs.u-strasbg.fr/">Studs</a> largement remaniée</p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-framadate/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'

    ],
    'mypads' => [
        'name'          => 'Développement et intégration du plugin Mypads',
        'wkp'           => '',
        'description'   => 'Rédaction collaborative',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<a href="https://framapad.org"><b class="violet">Frama</b><b class="vert">pad</b></a>',
        'date_frama'    => '2015',
        'coordonnees'   => '',
        'class'         => 'casque',
        'soft_frama'    => 'Etherpad',
        'long_desc'     => 'Du traitement de texte en ligne et à plusieurs !',
        'url_frama'     => 'https://framapad.org',
        'id_frama'      => 'framapad',
        'modale_title'  => 'Outil de rédaction collaborative',
        'modale_body'   => '<p>
                                <a href="https://framapad.org">Framapad</a> est un service de rédaction collaborative en ligne basé sur le logiciel
                                <a href="http://etherpad.org/">Etherpad</a>.
                            </p>
                            <p>
                                Un « pad » est un éditeur de texte en ligne. Son vrai plus ? L’édition collaborative : les contributions de chaque
                                utilisateur apparaissent immédiatement dans les pads de tous les participants, signalées par un code couleur.
                            </p>
                            <p class="violet">Fonctionnalités :</p>
                            <ul>
                                <li>Ouvrir un pad public ou privé</li>
                                <li>Rédiger votre document</li>
                                <li>Inviter des collaborateurs</li>
                                <li>Un code couleur pour chacun</li>
                                <li>Tchat intégré</li>
                                <li>Historique des versions</li>
                                <li>Exporter votre travail</li>
                                <li>Et bientôt : gérer des sessions et des groupes avec MyPads !</li>
                            </ul>
                            <p>
                                <b class="violet">Mise à jour</b> : cette mise à jour de Framapad sera conséquente, car en plus de bénéficier d’instances dédiées (par exemple à l’education ou aux sciences), vous pourrez (enfin !) gérer des pads privés (gestion par groupe ou par mot de passe).
                            </p>',
        'modale_footer' => '<p class="precisions">Framapad est une instance basée sur <a href="http://etherpad.org/">Etherpad</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-detherpad/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'

    ],
    'padlite' => [
        'name'          => 'Remplacement d’Etherpad par la version « Lite » pour les pads publics',
        'wkp'           => '',
        'description'   => 'Rédaction collaborative',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<a href="https://framapad.org"><b class="violet">Frama</b><b class="vert">pad</b></a>',
        'date_frama'    => '2013',
        'coordonnees'   => '',
        'class'         => 'casque',
        'soft_frama'    => 'Etherpad',
        'long_desc'     => 'Du traitement de texte en ligne et à plusieurs !',
        'url_frama'     => 'https://framapad.org',
        'id_frama'      => 'framapad',
        'modale_title'  => 'Outil de rédaction collaborative',
        'modale_body'   => '<p>
                                <a href="https://framapad.org">Framapad</a> est un service de rédaction collaborative en ligne basé sur le logiciel
                                <a href="http://etherpad.org/">Etherpad</a>.
                            </p>
                            <p>
                                Un « pad » est un éditeur de texte en ligne. Son vrai plus ? L’édition collaborative : les contributions de chaque
                                utilisateur apparaissent immédiatement dans les pads de tous les participants, signalées par un code couleur.
                            </p>
                            <p class="violet">Fonctionnalités :</p>
                            <ul>
                                <li>Ouvrir un pad public ou privé</li>
                                <li>Rédiger votre document</li>
                                <li>Inviter des collaborateurs</li>
                                <li>Un code couleur pour chacun</li>
                                <li>Tchat intégré</li>
                                <li>Historique des versions</li>
                                <li>Exporter votre travail</li>
                                <li>Et bientôt : gérer des sessions et des groupes avec MyPads !</li>
                            </ul>',
        'modale_footer' => '<p class="precisions">Framapad est une instance basée sur <a href="http://etherpad.org/">Etherpad</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-detherpad/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'

    ],
    'framindmap2' => [
        'name'          => 'Mindmaps remplacé par Wisemapping',
        'wkp'           => '',
        'description'   => 'Cartes heuristiques',
        'editeur'       => '',
        'wkp_editeur'   => '',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '',
        'alt_frama'     => '<a href="http://framindmap.org"><b class="violet">Fram</b><b class="vert">indmap</b></a>',
        'date_frama'    => '2014',
        'coordonnees'   => '',
        'class'         => 'casque',
        'soft_frama'    => 'Wisemapping',
        'long_desc'     => 'Fabriquez vos cartes mentales',
        'url_frama'     => 'http://framindmap.org',
        'id_frama'      => 'framindmap2',
        'modale_title'  => 'Pourquoi créer une carte mentale ?',
        'modale_body'   => '<p>
                                <a href="http://framindmap.org/">Framindmap</a> vous permet d’organiser un brainstorming, ordonner vos idées à plusieurs, apprendre et faire apprendre une leçon, réaliser des classifications, identifier les éléments importants.
                            </p>
                            <p>
                                <b class="violet">Pourquoi utiliser Framindmap ?</b> Il est utilisable en ligne, sans installation, aucune inscription n’est requise, vous pouvez exporter
                                votre document sous forme d’image, c’est un logiciel libre et gratuit, les données vous appartiennent.
                            </p>
                            <p>
                                <b class="violet">Mise à jour :</b> le logiciel qui propulse Framindmap sera Wisemapping (et non plus Mindmaps). Ce logiciel est plus complet, permet de lier des documents aux noeuds ou d’enregistrer vos cartes pour les retrouver et les partager facilement sur internet.
                            </p>',
        'modale_footer' => '<p class="precisions">Framindmap est une instance basée sur <a href="http://www.wisemapping.com/">Wisemapping</a></p><a href="http://framacloud.org/cultiver-son-jardin/installation-de-wisemapping/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> Installer</a>'
    ],
    'gmail' => [
        'name'          => 'Google Mail',
        'wkp'           => 'Gmail',
        'description'   => 'Service de messagerie',
        'editeur'       => 'Google',
        'wkp_editeur'   => 'Google',
        'similaire'     => '',
        'alt_online'    => '',
        'alt_offline'   => '<a href="http://mozilla.org/thunderbird">Thunderbird</a>',
        'alt_frama'     => '<b class="violet">Frama</b><b class="vert">mail</b>',
        'date_frama'    => '2017',
        'coordonnees'   => '',
        'class'         => '',
        'soft_frama'    => 'caliop',
        'long_desc'     => 'Vos mails, sans la NSA dedans',
        'url_frama'     => 'https://framamail',
        'id_frama'      => 'framamail',
        'modale_title'  => 'Votre courrier, dans une enveloppe blindée',
        'modale_body'   => '<p>
                                <a href="http://framadate.org">Framanews</a> vous propose un lecteur de flux RSS en ligne, vous permettant d’être toujours au courant de l’actualité à partir des flux RSS de vos sites préférés !
                            </p>
                            <p>
                                <b class="violet">Fonctionnement :</b> après avoir créé votre compte, enregistrez vos flux RSS et suivez-les.
                                Comment faire plus simple ?
                            </p>',
        'modale_footer' => '<p class="precisions">Framamail est une instance basée sur <a href="https://www.caliopen.org/">Caliopen</a></p>'
    ]
];

?>
